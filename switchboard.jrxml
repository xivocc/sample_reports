<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.11.0.final using JasperReports Library version 6.11.0-0c4056ccaa4d25a5a8c45672d2f764ea3498bebb  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="switchboard" pageWidth="960" pageHeight="842" orientation="Landscape" columnWidth="920" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" whenResourceMissingType="Error" isIgnorePagination="true" uuid="29273b48-368d-4bda-972a-1db70b62fb97">
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w1" value="15"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w2" value="985"/>
	<property name="com.jaspersoft.studio.property.dataset.dialog.DatasetDialog.sash.w1" value="1000"/>
	<property name="com.jaspersoft.studio.property.dataset.dialog.DatasetDialog.sash.w2" value="0"/>
	<property name="net.sf.jasperreports.export.csv.exclude.origin.band.1" value="summary"/>
	<property name="com.jaspersoft.studio.report.description" value=""/>
	<style name="Table_TH" mode="Opaque" backcolor="#F0F8FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
			<topPen lineWidth="0.5" lineColor="#000000"/>
			<leftPen lineWidth="0.5" lineColor="#000000"/>
			<bottomPen lineWidth="0.5" lineColor="#000000"/>
			<rightPen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="Table_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
			<topPen lineWidth="0.5" lineColor="#000000"/>
			<leftPen lineWidth="0.5" lineColor="#000000"/>
			<bottomPen lineWidth="0.5" lineColor="#000000"/>
			<rightPen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="Table 1_TH" mode="Opaque" backcolor="#F0F8FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
			<topPen lineWidth="0.5" lineColor="#000000"/>
			<leftPen lineWidth="0.5" lineColor="#000000"/>
			<bottomPen lineWidth="0.5" lineColor="#000000"/>
			<rightPen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<style name="Table 1_CH" mode="Opaque" backcolor="#BFE1FF">
		<box>
			<pen lineWidth="0.5" lineColor="#000000"/>
			<topPen lineWidth="0.5" lineColor="#000000"/>
			<leftPen lineWidth="0.5" lineColor="#000000"/>
			<bottomPen lineWidth="0.5" lineColor="#000000"/>
			<rightPen lineWidth="0.5" lineColor="#000000"/>
		</box>
	</style>
	<parameter name="date_debut" class="java.lang.String">
		<parameterDescription><![CDATA[]]></parameterDescription>
		<defaultValueExpression><![CDATA["2019-01-01"]]></defaultValueExpression>
	</parameter>
	<parameter name="date_fin" class="java.lang.String">
		<defaultValueExpression><![CDATA["2021-01-01"]]></defaultValueExpression>
	</parameter>
	<parameter name="granularite" class="java.lang.String">
		<defaultValueExpression><![CDATA["YYYY-MM-DD"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT
	ftime,
	queuefeatures.name,
	queuefeatures.displayname,
	COALESCE(global.offered,0) AS offered,
	COALESCE(global.answered,0) AS answered,
	COALESCE(global.abandoned, 0) AS abandoned,
	COALESCE(global.timeout,0) AS timeout,
	COALESCE(global.dissuaded,0) AS dissuaded,
	CASE WHEN global.offered > 0 THEN to_char(global.waittime/global.offered, 'HH24:MI:SS') ELSE '00:00:00' END AS avg_waittime,
	COALESCE(xfer.transfered,0) AS transfered
FROM

	(SELECT
		to_char(xc_qc.start_time::timestamp,$P{granularite}) AS ftime,
		queue_ref AS queue,
		SUM(CASE WHEN xc_qc.termination IN ('answered', 'leaveempty', 'timeout', 'abandoned') THEN 1 ELSE 0 END) AS offered,
		SUM(CASE WHEN xc_qc.termination IN ('timeout') THEN 1 ELSE 0 END) AS timeout,		
		SUM(CASE WHEN xc_qc.termination IN ('joinempty','leaveempty','divert_ca_ratio','divert_waittime') THEN 1 ELSE 0 END) AS dissuaded,
		SUM(CASE WHEN xc_qc.termination = 'answered' THEN 1 ELSE 0 END) AS answered,
		SUM(CASE WHEN xc_qc.termination = 'abandoned' THEN 1 ELSE 0 END) AS abandoned,
		CEIL(SUM(CASE WHEN xc_qc.termination IN ('answered', 'timeout', 'abandoned') THEN xc_qc.ring_duration ELSE 0 END)) * INTERVAL '1 seconds' AS waittime
	FROM xc_queue_call xc_qc
	WHERE xc_qc.start_time BETWEEN ($P{date_debut} || ' 00:00:00')::timestamp AND ($P{date_fin} || ' 23:59:59')::timestamp
	GROUP BY ftime, queue_ref) global
NATURAL FULL JOIN
		
	(SELECT
		to_char(xc_qc.start_time::timestamp,$P{granularite}) AS ftime,
		queue_ref AS queue,
		COUNT(xc_cc.answer_time) AS transfered
	FROM xc_call_channel xc_cc
		JOIN xc_call_channel xc_cc_initial ON xc_cc_initial.id = xc_cc.original_call_id
		JOIN xc_call_transfer xc_ct ON xc_ct.onhold_call_id = xc_cc.id OR xc_ct.onhold_call_id = xc_cc_initial.id 
		JOIN xc_queue_call xc_qc ON xc_qc.id = xc_cc_initial.queue_call_id
	WHERE xc_qc.start_time BETWEEN ($P{date_debut} || ' 00:00:00')::timestamp AND ($P{date_fin} || ' 23:59:59')::timestamp
	GROUP BY ftime, queue) xfer


JOIN queuefeatures ON queue = queuefeatures.name
WHERE queuefeatures.name SIMILAR TO '%(_switchboard|_hold)|%_hold_[0-9]+'
ORDER BY ftime, queuefeatures;]]>
	</queryString>
	<field name="ftime" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="ftime"/>
	</field>
	<field name="name" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="name"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="queuefeatures"/>
	</field>
	<field name="displayname" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="displayname"/>
		<property name="com.jaspersoft.studio.field.tree.path" value="queuefeatures"/>
	</field>
	<field name="offered" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.label" value="offered"/>
	</field>
	<field name="answered" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.label" value="answered"/>
	</field>
	<field name="abandoned" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.label" value="abandoned"/>
	</field>
	<field name="timeout" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.label" value="timeout"/>
	</field>
	<field name="dissuaded" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.label" value="dissuaded"/>
	</field>
	<field name="avg_waittime" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="avg_waittime"/>
	</field>
	<field name="transfered" class="java.lang.Long">
		<property name="com.jaspersoft.studio.field.label" value="transfered"/>
	</field>
	<variable name="totv_offered" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{offered}]]></variableExpression>
	</variable>
	<variable name="totv_answered" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{answered}]]></variableExpression>
	</variable>
	<variable name="totv_abandoned" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{abandoned}]]></variableExpression>
	</variable>
	<variable name="totv_transfered" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{transfered}]]></variableExpression>
	</variable>
	<variable name="totv_dissuaded" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{dissuaded}]]></variableExpression>
	</variable>
	<variable name="totv_timeout" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{timeout}]]></variableExpression>
	</variable>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<property name="com.jaspersoft.studio.unit.height" value="px"/>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="90" y="-20" width="150" height="30" backcolor="#D6D6D6" uuid="4ed6e844-8f4b-41a6-8608-9c342bfc13a0"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Files Switchboard]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="240" y="-20" width="100" height="30" backcolor="#9AA8B5" uuid="b82a234a-ff74-4b78-8a88-9fd88e4e43e7">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Présentés]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="340" y="-20" width="100" height="30" backcolor="#7F97AD" uuid="e0a7e543-abcb-4f6f-8575-49335c36ecd4">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Répondus]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="540" y="-20" width="100" height="30" backcolor="#7F97AD" uuid="1bab0ec8-dcd8-4934-b65d-599eac42c9f7">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Non répondus]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="440" y="-20" width="100" height="30" backcolor="#7F97AD" uuid="75503a16-3277-42a3-9f3a-8915ebd26a91">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Abandonnés]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="-20" y="-20" width="110" height="30" backcolor="#D6D6D6" uuid="408d0ce9-d75f-43f5-8dc6-bbd59ae34b17"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Date]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="640" y="-20" width="100" height="30" backcolor="#BDA1A0" uuid="45105950-9802-47ca-9657-fbee08061380">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Dissuadés]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="740" y="-20" width="100" height="30" backcolor="#A2B891" uuid="3f3afff4-f2f2-4ce3-a60e-95445589e0ba">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Transférés]]></text>
			</staticText>
			<staticText>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="840" y="-20" width="100" height="30" backcolor="#9BBAB5" uuid="717b137e-3448-43a9-8120-3d8de3532d38">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Temps d'attente moyen]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<property name="com.jaspersoft.studio.unit.height" value="px"/>
			<property name="com.jaspersoft.studio.layout"/>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" mode="Opaque" x="90" y="-10" width="150" height="20" backcolor="#F5F5F5" uuid="b2aa51f0-d446-40a3-8d27-16737c83b8a4"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{displayname}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" isPrintRepeatedValues="false" mode="Opaque" x="-20" y="-10" width="110" height="20" backcolor="#F5F5F5" uuid="9f3de3d4-00ff-49c7-be37-2e54a4122cda"/>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" rotation="None">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ftime}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="240" y="-10" width="100" height="20" uuid="a686d88e-2da0-4402-ac08-1e354b4f95ac">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{offered}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="340" y="-10" width="100" height="20" uuid="a03ab87a-7e26-4d2c-bc51-2baa1a63cb7d">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{answered}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="540" y="-10" width="100" height="20" uuid="a3831c6b-b031-45cd-ae10-efa0f854580a">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{name}.matches("\\w*_hold*")) ? "-" : $F{timeout}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="440" y="-10" width="100" height="20" uuid="9a05e0b6-59ac-41a3-add2-88a1fb19955b">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{abandoned}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="640" y="-10" width="100" height="20" uuid="b68732dd-08d9-4bb4-81d2-bed3b986b329">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{name}.matches("\\w*_hold*")) ? "-" : $F{dissuaded}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="740" y="-10" width="100" height="20" uuid="62beb400-c8ef-4034-893e-be1d024dfe8a">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[($F{name}.matches("\\w*_hold*")) ? "-" : $F{transfered}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement stretchType="RelativeToTallestObject" x="840" y="-10" width="100" height="20" uuid="6bc64673-9f9e-46f7-a584-cb7daee0ce88">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{avg_waittime}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="580" splitType="Stretch">
			<staticText>
				<reportElement mode="Opaque" x="-20" y="-10" width="260" height="30" backcolor="#C4C4C4" uuid="85236ed1-413d-4e6b-9acd-a8badc85d807"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
			<textField>
				<reportElement mode="Opaque" x="240" y="-10" width="100" height="30" backcolor="#C4C4C4" uuid="0f2bb857-b106-46bd-a419-8ff03915340e">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totv_offered}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="340" y="-10" width="100" height="30" backcolor="#C4C4C4" uuid="08abb70e-1688-470d-9363-aee25abdece8">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totv_answered}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="540" y="-10" width="100" height="30" backcolor="#C4C4C4" uuid="0ecd67b5-cefd-4c59-8051-e28dc1e5232d">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totv_timeout}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="440" y="-10" width="100" height="30" backcolor="#C4C4C4" uuid="54f62d70-db42-412a-8b75-3c728b012b90">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totv_abandoned}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement mode="Opaque" x="-5" y="70" width="83" height="20" backcolor="#F5F5F5" uuid="a4da68e4-fb9b-407b-a63d-28240d341513"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="DejaVu Sans" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Période:]]></text>
			</staticText>
			<textField>
				<reportElement x="78" y="70" width="227" height="20" uuid="052a56a6-14d4-4f40-96b2-ad20d86e9a41">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["   Du "+$P{date_debut}+" au  "+$P{date_fin}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="-5" y="40" width="310" height="30" uuid="0d6f0250-06aa-43c3-b1f1-7c061d9b687a"/>
				<textElement verticalAlignment="Middle">
					<font size="14" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Paramètres :]]></text>
			</staticText>
			<staticText>
				<reportElement x="-6" y="166" width="946" height="75" uuid="eadf736a-bd9e-4d16-a407-edcb68c77fc3">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Monospaced" size="9"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Présentés    : Nombre d'appels offerts à la file (sont exclus les appels en dehors de l'horaire d'ouverture et les appels dissuadés)
Répondus     : Nombre d'appels répondus
Abandonnés   : Nombre d'appels abandonnés par l'appelant dans la file d'attente
Non répondus : Nombre d'appels ayant atteint le temps d'expiration d'attente
Dissuadés    : Nombre d'appels dissuadés (horaire, maximum d'appels ...) et donc non présentés à la file
Transférés   : Nombre d'appels répondus puis transférés à une autre destination
]]></text>
			</staticText>
			<staticText>
				<reportElement x="-5" y="136" width="310" height="30" uuid="07e35917-cc8a-49e4-bd1f-f5f82806c760"/>
				<textElement verticalAlignment="Middle">
					<font size="14" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Légende :]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="387" y="70" width="83" height="20" backcolor="#F5F5F5" uuid="f60bb05c-1b9d-4042-bf8a-02d0323178bd"/>
				<box>
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="DejaVu Sans" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Granularité:]]></text>
			</staticText>
			<textField>
				<reportElement x="470" y="70" width="227" height="20" uuid="3e0d8531-e5ce-4f1e-8c3d-e7a1e4109c6b">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<box topPadding="0" leftPadding="0" bottomPadding="0" rightPadding="0">
					<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement verticalAlignment="Middle">
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["   "+IF(EQUALS($P{granularite},"YYYY-MM-DD"),"Jour",IF(EQUALS($P{granularite},"YYYY"),"Année",IF(EQUALS($P{granularite},"YYYY-MM"),"Mois",IF(EQUALS($P{granularite},"YYYY-IW"),"Semaine",IF(EQUALS($P{granularite},"YYYY-MM-DD HH24:00"),"Heure",$P{granularite}))))) ]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="640" y="-10" width="100" height="30" backcolor="#C4C4C4" uuid="2f9ab95f-4a80-46ff-9e99-c6609ad6749a">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totv_dissuaded}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement mode="Opaque" x="740" y="-10" width="100" height="30" backcolor="#C4C4C4" uuid="1ace37bd-10fe-497c-8d91-640b54b4e2ef">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totv_transfered}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="-5" y="241" width="93" height="30" uuid="3ce45da7-e131-467e-8062-1b44894f409f"/>
				<textElement verticalAlignment="Middle">
					<font size="14" isBold="true"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Version :]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="88" y="246" width="83" height="21" forecolor="#555555" uuid="eb5733e2-ac10-4055-babc-1e17cde9eb32"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="SansSerif" size="14" isBold="false" isItalic="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[1.0]]></text>
			</staticText>
			<textField>
				<reportElement mode="Opaque" x="840" y="-10" width="100" height="30" backcolor="#C4C4C4" uuid="112c2ced-03af-40c9-ab19-41a75a77efe0">
					<property name="com.jaspersoft.studio.unit.width" value="px"/>
				</reportElement>
				<box>
					<topPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
					<rightPen lineWidth="0.5" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="10" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["-"]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
