#!/bin/bash

set -e

cmn_echo_info() {
  local green=$(tput setaf 2)
  local reset=$(tput sgr0)
  echo -e "${green}$@${reset}"
}

cmn_echo_important() {
  local yellow=$(tput setaf 3)
  local reset=$(tput sgr0)
  echo -e "${yellow}$@${reset}"
}

cmn_echo_warn() {
  local red=$(tput setaf 1)
  local reset=$(tput sgr0)
  echo -e "${red}$@${reset}"
}

function usage {
    cat <<EOF

Usage: extract_report [-h] <report.jrxml>
	   - -h, --help displays this help
	   - <report.jrxml> the report you want to extract the SQL request from as a fully pastable/usable request.

SQL DOESN'T UNDERSTAND JASPER PARMETERS. The function replace_parameters below is here to help with that.
EDIT replace_parameters with your wished input:

\$P{date_debut} => the start of the report, MUST BE A DAY, format year-month-day, feel free to update default
\$P{date_fin} => the end of the report, MUST BE A DAY, format year-month-day, feel free to update default

5 granularities are availables, depending of which you MUST set what \$P{granularity} and \$P{trunc_date_to} will be replaced by:
       |   \$P{granularity}   | \$P{trunc_date_to}  |
month  |          MM         |       month        |
weeks  |          IW         |       week         |
days   |      YYYY-MM-DD     |       day          |
hours  |YYYY-MM-DD HH24:00:00|       hour         |
15mins |YYYY-MM-DD HH:MI:SS  |       15min        |   

IF some jasper parameters remains, edit them manually/complete the replace_parameters function.
Parameters starting with \$! are lists, and cannot be replaced by this script. handle manually (usually remove the conditions)
EOF
    exit 1
}

verify_help_asked() {
    if [ "$1" == "-h" ] || [ "$1" == "--help" ] ; then
        usage
    fi
}

verify_help_asked "$1"

input_file="$1"

if [ -z "${input_file}" ] || ! grep -q ".jrxml" <<< "${input_file}";then
    cmn_echo_warn "no report given"
    usage
else
    output_file=${input_file%\.jrxml}'.sql'
fi
if ! [ -f "${input_file}" ];then
    cmn_echo_warn "report ${input_file} does not seem to exist"
    usage
fi

extract_request(){
    if [ -f "${output_file}" ];then
        rm "$output_file"
    fi
    touch "$output_file"
    awk '/<queryString language="SQL">/,/;/' "$input_file" > "$output_file"
    sed -i '1d' "$output_file"
    sed -i 's/^.*<!\[CDATA\[//g' "$output_file"
    sed -i 's/\]\]>//g' "$output_file"
}

replace_parameters(){
    # shellcheck disable=SC2016
    sed -i -e 's/\$P{granularite}/'\''YYYY-MM-DD'\''/g' "$output_file"
    # shellcheck disable=SC2016
    sed -i -e 's/\$P{trunc_date_to}/'\''day'\''/g' "$output_file"
    # shellcheck disable=SC2016
    sed -i -e 's/\$P{date_debut}/'\''2024-06-29'\''/g' "$output_file"
    # shellcheck disable=SC2016
    sed -i -e 's/\$P{date_fin}/'\''2024-08-04'\''/g' "$output_file"
}

remaining_parameters_or_values(){
    if grep -q "\$P" "$output_file";then
        cmn_echo_warn "some variables remains:"
        grep -n --color "\$P" "$output_file"
        cmn_echo_important "report has been extracted under $output_file, BUT you must fix it to execute it"
        exit 1
    fi
}

extract_request
replace_parameters
remaining_parameters_or_values

cmn_echo_info "Congratulations, report has been extracted under $output_file"
