# Sample BI reports

This package provides fully functional samples to help you to build your own BI reports.
They currently aimed to be deployed on SpagoBI solution, but as they are simple Jrxml files, they can be imported easily in JasperSoft solutions.

## Report version table

The latest versions available for each report.

Report | Version
--- | ---
Queue Support | 4.5
Agent Support | 4.6
Agent Reduced | 4.5

Note: please do not edit the names of agent_support.zip and agent_reduced.zip as they are hardlinked to the doc. The same name must be defined at export time.

## Documentation

Here an exhaustive list of sample reports shipped:

### Queue Support Report

* `"queue_support.jrxml"` : Queue-based report : the queue call distribution per day
It is based on the 'xc_' stats model and the old queue.jrxml report
![queue_support](screenshots/queue_support.png)

#### Queue Support Availability per LTS

:warning: Before using this report you must ensure that it is available for your version. **The data will only be correct if you are in the LTS Bugfix version listed below.**

Availaibility per LTS :

LTS | Available | Since
--- | --- | ---
Maia | :white_check_mark: | IV 2024.05.03
Luna | :white_check_mark: | IV 2023.08.00
Kuma | :white_check_mark: | Kuma.06
Jabbah | :white_check_mark: | Jabbah.10
Izar | :white_check_mark: | Izar.21
Helios | :white_check_mark: | Helios.22
Gaia | :x: | Gaia.xx
Freya | :white_check_mark: | Freya.16

#### Import queue_support to SpagoBI

1. Retrieve the [queue_support_report.zip](./spagobi/queue_support_report.zip)
1. Login to SpagoBI, go to the Import/Export section.
1. Select Import, choose the zip file. 
1. Select the top right double arrows until the end without changing anything.
1. => Then the report is available among the others near the 'Statistiques FA' report.

The report will work for the data generated after you upgraded to one of the version above.
Once you upgraded to a version listed above you might want to fix the data generated before.
For this you must use the `xivocc-insert-unoffered-events`. This script is available on XiVO CC. See `xivocc-insert-unoffered-events -h` for how to use it.

### Agent Support Report

`"agent.jrxml"` : Agent-based report containing :

* the queue calls received by agents
* the outgoing calls made by agents
* the login, pause and wrapup times of the agents

It is based on the 'xc_' stats model, the queue_log and call_data for outgoing calls

It was inspired by the previous agent report (deprecated since Luna)
![agent_support](screenshots/agent_support.png)

#### Agent Support Availability per LTS

The report requires that the data replicated on the XiVOCC are correct. It can only provide datas as old as the stats you kept there. In theory the report might work as old as Deneb ? We did not test that far.

#### Import agent_support to SpagoBI

1. Retrieve the zip under agent_support_ in folder spagobi/
1. Login to SpagoBI, go to the Import/Export section.
1. Select Import, choose the zip file.
1. Select the top right double arrows until the end without changing anything.
1. => Then the report is available as 'Agent_Support' under functionnalities>Raports>Examples.

### Agent Reduced Report

A reduced version of the Agent Support Report focusing on the queue calls only. In theory faster to execute.

![agent_support](screenshots/agent_reduced.png)

Since it focuses on queue calls it is possible to update this report so that it also sorts data by queues.

### System Info

* `"systeminfo.jrxml"` : State of the datasources
![systeminfo](screenshots/systeminfo.png)

## Development

Some reports are using scriplets, scriptlets must be build from the `packaging/spagobi/scriptlets` project and installed to jasper studio

### Install scriptlets in Jasper Studio

 Menu project -> Properties -> Java Build Path

 Add xivo-scriplet-X.X.jar as an external library

### Versioning

In case of update of a sample reports, update version number in jrxml so that correct version can be display to user.

### Packaging

A compiled zip version of reports must be available in spagobi folder named **samples_from_borealis_vxx** where xx is the version of the most recent report.

In order to generate the zip file:

* Stop spagobi container and remove it
    ```bash
    xivocc-dcomp stop spagobi
    xivocc-dcomp rm spagobi
    ```
* Drop spagobi database in pgxivocc
    ```bash
    xivocc-dcomp exec pgxivocc psql -U postgres
    postgres=# DROP DATABASE spagobi;
    ```
* Recreate spagobi database
    ```bash
    xivocc-dcomp exec pgxivocc psql -U postgres
    postgres=#CREATE DATABASE spagobi WITH OWNER spagobi;
    ```
* Recreated spagobi container
    ```bash
    xivocc-dcomp up -d spagobi
    ```
* Import last `samples_from_borealis_vxx` from sample zip folder
* Update the reports directly in your instance of spagobi by uploading your updated .jrxml
  * in SpagoBI click on the 'eye' :eye: icon
  * and then import new report by choosing your updated .jrxml file in **Modèle** parameter
* Export the zip with updated version number according format defined above and copy it in spagobi folder, push your changes in git
  * Go to **Import/Export** menu
  * In *Nom export* type the export file name (without `.zip`) : e.g. samples_from_borealis_vXX
  * Open the object tree and select all the reports under the *Rapports* folder
  * And click on Export
* Update the documentation to point to the latest version of the reports in the version where it applies

See [SpagoBI Installation](https://documentation.xivo.solutions/en/latest/installation/xivocc/installation/installation.html#spagobi)

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING and COPYING.LESSER files for details.
